class Api::UsersController < ApplicationController
  def create
    @user = User.new(user_params)
    if @user.save
      login(@user)
    else
      # flash.now[:errors] = @user.errors.full_messages #old way.
      render :json => { :errors => @user.errors.full_messages }, :status => 422
    end
  end

  def show
    
  end

  def user_params
    params.require(:user).permit(:username, :password)
  end

end
