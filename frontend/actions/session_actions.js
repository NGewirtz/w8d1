import * as ApiUtil from '../util/session_api';

export const RECEIVE_CURRENT_USER = 'RECEIVE_CURRENT_USER';
export const RECEIVE_SESSION_ERRORS = 'RECEIVE_SESSION_ERRORS';

const receiveSessionErrors = (errors) => {
  return {
    type: RECEIVE_SESSION_ERRORS,
    errors
  };
};

const receiveCurrentUser = (currentUser) => {
  return {
    type: RECEIVE_CURRENT_USER,
    currentUser
  };
};

export const logIn = (user) => (dispatch) => {
  return ApiUtil.logIn(user).then((currentUser) => {
    dispatch(receiveCurrentUser(currentUser));
  }, (errors) => {
    dispatch(receiveSessionErrors(errors));
  });
};

export const logOut = () => (dispatch) => {
  return ApiUtil.logOut().then(() => {
    dispatch(receiveCurrentUser(null));
  }, (errors) => {
    dispatch(receiveSessionErrors(errors));
  });
};

export const signUp = (user) => (dispatch) => {
  return ApiUtil.signUp(user).then((newUser) => {
    dispatch(receiveCurrentUser(newUser));
  }, (errors) => {
    dispatch(receiveSessionErrors(errors));
  });
};
