import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
// import logger from 'redux-logger';

import RootReducer from '../reducers/root_reducer';

const defaultState = {
  session: {
    currentUser: null,
  },
  errors: {
    session: []
  }
};

const configureStore = (preloadedState) => {
  preloadedState = preloadedState || defaultState;
  return createStore(RootReducer, preloadedState,   applyMiddleware(thunk));
};

export default configureStore;
