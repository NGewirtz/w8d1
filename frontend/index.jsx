import React from 'react';
import ReactDOM from 'react-dom';
import { signUp, logIn, logOut } from './actions/session_actions';
import Root from './components/root';
import configureStore from './store/store';
import { fetchBenches } from './actions/bench_actions';



document.addEventListener("DOMContentLoaded", () => {
  let store;
  if (window.currentUser) {
    const preloadedState = { session: { currentUser: window.currentUser } };
    store = configureStore(preloadedState);
  } else {
    store = configureStore();
  }
  const root = document.getElementById('root');
  window.store = store;
  window.getState = store.getState;
  window.dispatch = store.dispatch; // just for testing!
  window.signUp = signUp;
  window.logIn = logIn;
  window.logOut = logOut;
  window.fetchBenches = fetchBenches;
  ReactDOM.render(<Root store = {store}/>, root);
});
