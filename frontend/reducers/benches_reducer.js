import { RECEIVE_BENCHES } from '../actions/bench_actions';

const BenchesReducer = (state = {}, action) => {
  Object.freeze(state);
  switch(action.type) {
    case RECEIVE_BENCHES:
      // action.benches.forEach(bench => {
      //   newBenches[bench.id] = bench;
      // });
      return Object.assign({}, state, action.benches);
    default:
      return state;
  }
};

export default BenchesReducer;
