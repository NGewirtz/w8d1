import { combineReducers } from 'redux';
import FilterReducer from './filter_reducer';

const UiReducer = combineReducers({
  filters: FilterReducer
});

export default FilterReducer;
