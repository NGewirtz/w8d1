import React from 'react';
import BenchMap from './bench_map';
import BenchIndex from './bench_index';

const Search = (props) => {
  return (
    <div>
      <BenchIndex benches={props.benches} fetchBenches={props.fetchBenches}/>
      <BenchMap benches={props.benches} fetchBenches={props.fetchBenches} updateFilter={props.updateFilter}/>
    </div>
  );
};


export default Search;
