import { connect } from 'react-redux';
import { logOut } from '../actions/session_actions';
import Greeting from './greeting';


const mapStateToProps = (state) => {
  return {
    currentUser: state.session.currentUser
  };
};


const mapDispatchToProps = (dispatch) => {
  return {
    logout: () => dispatch(logOut())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Greeting);
