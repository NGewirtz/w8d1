import React from 'react';
import { Provider } from 'react-redux'
import { HashRouter, Route } from 'react-router-dom';
import configureStore from '../store/store';
import App from './App';


const Root = ({ store }) => (
  <Provider store={store}>
    <HashRouter>
      <App />
    </HashRouter>
  </Provider>
);

export default Root;
