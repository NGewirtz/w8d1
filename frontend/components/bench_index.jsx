import React from 'react';
import BenchIndexItem from './bench_index_item';

class BenchIndex extends React.Component {

  componentDidMount() {
  }

  render() {
    const benches = this.props.benches.map( (bench, idx) => {
      return <BenchIndexItem key={idx} bench={bench} />;
    });
    return (
      <ul>
        { benches }
      </ul>
    );
  }
}

export default BenchIndex;
