import { connect } from 'react-redux';
import Search from './search';
import { fetchBenches } from '../actions/bench_actions';
import { benchesSelector } from '../reducers/selectors';
import { updateFilter } from '../actions/filter_actions';

const mapStateToProps = (state) => {
  return {
    benches: benchesSelector(state.entities.benches)
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchBenches: (bounds) => dispatch(fetchBenches(bounds)),
    updateFilter: (bounds) => dispatch(updateFilter(bounds))
  };
};



export default connect(mapStateToProps, mapDispatchToProps)(Search);
