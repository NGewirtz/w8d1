import React from 'react';
import { withRouter } from 'react-router-dom';

class SessionForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: ""
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();
    const user = Object.assign({}, this.state);
    this.props.processForm(user);
  }

  handleChange(key) {
    return (e) => {
      this.setState({ [key]: e.target.value });
    };
  }

  render() {
    const errors = this.props.errors.session.map(error => (
      <li>{error.responseText}</li>
    ));
    return (
      <form onSubmit={this.handleSubmit}>
        <h1>{this.props.formType}</h1>
        <ul>
          {errors}
        </ul>
        <input type="text" onChange={this.handleChange('username')}
          value={this.state.username} />
        <input type="text" onChange={this.handleChange('password')}
          value={this.state.password}/>
        <button>Submit</button>
      </form>
    );
  }
}

export default withRouter(SessionForm);
