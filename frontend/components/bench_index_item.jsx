import React from 'react';

const BenchIndexItem = ({ bench }) => (
  <li> {bench.description} | lat {bench.lat} | lng {bench.lng}  </li>
);



export default BenchIndexItem;
