import React from 'react';
import MarkerManager from '../util/marker_manager';

class BenchMap extends React.Component {

  componentDidMount() {
    const mapOptions = {
      center: { lat: 37.7758, lng: -122.435 }, // this is SF
      zoom: 13
    };
    let bounds;
    this.map = new google.maps.Map(this.mapNode, mapOptions);
    this.MarkerManager = new MarkerManager(this.map);
    this.MarkerManager.updateMarkers(this.props.benches);
    this.map.addListener('idle', () => {
      let newBounds = this.map.getBounds();
      let neLat = newBounds.getNorthEast().lat();
      let neLng = newBounds.getNorthEast().lng();
      let swLat = newBounds.getSouthWest().lat();
      let swLng = newBounds.getSouthWest().lng();
      bounds = {
        "northEast": {"lat": neLat, "lng": neLng},
        "southWest": {"lat": swLat, "lng": swLng}
      };
      this.props.updateFilter(bounds);
    });
  }

  componentWillReceiveProps() {
    this.MarkerManager.updateMarkers(this.props.benches);
  }

  render() {
    return (
      <div id="map-container" ref={ map => this.mapNode = map }>

      </div>
    );
  }

}

export default BenchMap;
