import { connect } from 'react-redux';
import BenchIndex from './bench_index';
import { fetchBenches } from '../actions/bench_actions';
import { benchesSelector } from '../reducers/selectors';

const mapStateToProps = (state) => {
  return {
    benches: benchesSelector(state.entities.benches)
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchBenches: (bounds) => dispatch(fetchBenches(bounds))
  };
};



export default connect(mapStateToProps, mapDispatchToProps)(BenchIndex);
