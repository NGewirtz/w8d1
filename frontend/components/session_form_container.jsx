import { connect } from 'react-redux';
import SessionForm from './session_form';
import { logIn, signUp } from '../actions/session_actions';

const mapStateToProps = (state, ownProps) => {
  const loggedIn = state.session.currentUser ? true : false;
  const formType = ownProps.location.pathname === '/login' ? 'login' : 'signup';
  return {
    loggedIn,
    errors: state.errors,
    formType
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  let formAction;
  if (ownProps.location.pathname === '/login') {
    formAction = (user) => dispatch(logIn(user));
  }else {
    formAction = (user) => dispatch(signUp(user));
  }
  return {
    processForm: formAction
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SessionForm);
