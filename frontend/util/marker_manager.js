export default class MarkerManager {
  constructor(map) {
    this.map = map;
    this.markers = {};
  }

  updateMarkers(benches) {
    console.log('time to update');
    const benchObj = {};
    benches.forEach(bench => {
      this.createMarkerFromBench(bench);
      benchObj[bench.id] = bench;
    });
    // Object.keys(this.markers).forEach((id)=> {
    //   if(benchObj[id] === undefined) {
    //     delete this.markers[id];
    //   }
    // });
  }

  createMarkerFromBench(bench) {
    this.markers[bench.id] = bench;
    const lat = parseFloat(bench.lat);
    const lng = parseFloat(bench.lng);
    const position = { lat, lng };
    const marker = new google.maps.Marker({
      position,
      title: 'marker'
    });
    marker.setMap(this.map);
  }

  removeMarker(marker) {

  }
}
