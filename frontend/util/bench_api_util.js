export const fetchBenches = (filters) => {
  return $.ajax({
    method: 'get',
    url: `api/benches/?bounds=${JSON.stringify(filters)}`,
    error: (err) => console.log(err),
  });
};
